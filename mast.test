<?php

/**
 * @file
 * Tests for remote content services.
 */

class MAStTest extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => t('MASt tests'),
      'description' => t('Tests oAuth login and access to remote content services.'),
      'group' => t('MASt'),
    );
  }

  function setUp() {
    $modules = array('mast');
    parent::setUp($modules);

    $this->endUser = $this->drupalCreateUser(array('create article content', 'oauth authorize any consumers'));

    // Create an administrative user who owns a consumer API key and secret.
    $this->consumerOwner = $this->drupalCreateUser(array());
    $options = array(
      'callback_url' => 'oob', // In real life, this would be the mobile app.
      'uid' => $this->consumerOwner->uid,
      'provider_consumer' => TRUE,
      'in_database' => TRUE,
      'context' => 'remote_content_services',
    );
    $this->consumer = new DrupalOAuthConsumer(user_password(32), user_password(32), $options);
    $this->consumer->write();

    $this->signatureMethod = new OAuthSignatureMethod_HMAC('SHA1');

    $this->accessToken = $this->getAccessToken($this->endUser);
  }

  /**
   * Helper function; returns an oAuth access token.
   *
   * @param $user
   *   The user for which to create the token.
   *
   * @return DrupalOAuthToken
   *   A valid oAuth access token.
   */
  function getAccessToken($user) {
    // Log in as the end user who will authorize the mobile app.
    $this->drupalLogin($user);

    // Get a request token (first step in oAuth).
    $request = OAuthRequest::from_consumer_and_token($this->consumer, NULL, 'GET', $this->getAbsoluteUrl('/oauth/request_token'));
    $request->sign_request($this->signatureMethod, $this->consumer, NULL);
    $requestURL = $request->to_url();

    $result = $this->drupalGet($requestURL);
    $this->assertResponse('200');
    parse_str($result, $request_token_parameters);

    // Authorize the application (second step in oAuth).
    $request_token = new DrupalOAuthToken($request_token_parameters['oauth_token'], $request_token_parameters['oauth_token_secret'], $this->consumer);
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $request_token, 'GET', $this->getAbsoluteUrl('/oauth/authorize'));
    $request->sign_request($this->signatureMethod, $this->consumer, $request_token);
    $requestURL = $request->to_url();
    // We need to follow the redirect after authorizing, so we use curlExec()
    // here, which lets us specify our own options (including FOLLOWLOCATION).
    $out = $this->curlExec(array(
      CURLOPT_HTTPGET => TRUE,
      CURLOPT_URL => $requestURL,
      CURLOPT_NOBODY => FALSE,
      CURLOPT_HTTPHEADER => array(),
      // Because we specified automatic_authorization in the default oAuth
      // context (see mast_default_oauth_common_context()), all we need to do is
      // follow the redirect specified in the consumer. If we hadn't specified
      // automatic_authorization, we would need to find the "Grant access"
      // button on /oauth/authorize and use it to submit the form.
      CURLOPT_FOLLOWLOCATION => TRUE,
    ));
    // Ensure that any changes to variables in the other thread are picked up.
    $this->refreshVariables();
    $this->verbose('<pre>' . $out . '</pre>');

    // Now that the end user has authorized us, we can get an access token
    // (third step in oAuth).
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $request_token, 'GET', $this->getAbsoluteUrl('/oauth/access_token'));
    $request->sign_request($this->signatureMethod, $this->consumer, $request_token);
    $requestURL = $request->to_url();
    $result = $this->drupalGet($requestURL);
    $this->assertResponse('200');
    parse_str($result, $access_token_parameters);

    $access_token = new DrupalOAuthToken($access_token_parameters['oauth_token'], $access_token_parameters['oauth_token_secret'], $this->consumer);

    // Log out, to avoid any confusion about whether we're accessing things
    // via the session instead of via oAuth.
    $this->drupalLogout();

    return $access_token;
  }

  /**
   * Tests that the field listing exists and can be accessed.
   */
  function testIndex() {
    // Try to retrieve the page without an oAuth access token.
    $this->drupalGet('/content-types/fields');
    $this->assertResponse('401');
    // Retrieve the page with an authorized access token.
    $this->oAuthGet('/content-types/fields');
    $this->assertResponse('200');
  }

  /**
   * Helper function for making a GET request using an access token.
   *
   * @param $path
   *   The path to be requested, relative to the Drupal root.
   */
  function oAuthGet($path) {
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $this->accessToken, 'GET', $this->getAbsoluteUrl($path), NULL);
    $request->sign_request($this->signatureMethod, $this->consumer, $this->accessToken);
    $requestURL = $request->to_url();

    $out = $this->curlExec(array(
      CURLOPT_HTTPGET => TRUE,
      CURLOPT_URL => $requestURL,
      CURLOPT_NOBODY => FALSE,
      CURLOPT_HTTPHEADER => array(),
    ));
    // Ensure that any changes to variables in the other thread are picked up.
    $this->refreshVariables();
    $this->verbose('<pre>' . $out . '</pre>');
    return $out;
  }

  /**
   * Helper function for making a POST request using an access token.
   *
   * @param $path
   *   The path to be requested, relative to the Drupal root.
   * @param $data
   *   An array of POST data, appropriate for http_build_query().
   * @param $options
   *   Optional. An associative array of cURL options.
   *
   * @return
   *   The result of the cURL request.
   */
  function oAuthPost($path, $data, $options = array()) {
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $this->accessToken, 'POST', $this->getAbsoluteUrl($path), $data);
    $request->sign_request($this->signatureMethod, $this->consumer, $this->accessToken);

    $defaults = array(
      CURLOPT_URL => $request->to_url(),
      CURLOPT_POST => TRUE,
      CURLOPT_POSTFIELDS => http_build_query($request->get_parameters(), '', '&'),
    );
    $options = $defaults + $options;
    $out = $this->curlExec($options);
    // Ensure that any changes to variables in the other thread are picked up.
    $this->refreshVariables();
    $this->verbose('<pre>' . $out . '</pre>');
    return $out;
  }

  /**
   * Helper function for making a PUT request using an access token.
   *
   * @param $path
   *   The path to be requested, relative to the Drupal root.
   * @param $data
   *   The data to be put.
   *
   * @return
   *   The result of the cURL request.
   */
  function oAuthPut($path, $data) {
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $this->accessToken, 'PUT', $this->getAbsoluteUrl($path), $data);
    $request->sign_request($this->signatureMethod, $this->consumer, $this->accessToken);
    $options = array(
      CURLOPT_CUSTOMREQUEST => 'PUT',
      CURLOPT_URL => $request->to_url(),
      CURLOPT_POST => FALSE,
      CURLOPT_POSTFIELDS => http_build_query($request->get_parameters(), '', '&'),
    );
    $out = $this->curlExec($options);
    // Ensure that any changes to variables in the other thread are picked up.
    $this->refreshVariables();
    $this->verbose('<pre>' . $out . '</pre>');
    return $out;
  }

  /**
   * Helper function for making a DELETE request using an access token.
   *
   * @param $path
   *   The path to be requested, relative to the Drupal root.
   *
   * @return
   *   The result of the cURL request.
   */
  function oAuthDelete($path) {
    $request = OAuthRequest::from_consumer_and_token($this->consumer, $this->accessToken, 'DELETE', $this->getAbsoluteUrl($path), NULL);
    $request->sign_request($this->signatureMethod, $this->consumer, $this->accessToken);
    $requestURL = $request->to_url();

    $out = $this->curlExec(array(
      CURLOPT_CUSTOMREQUEST => 'DELETE',
      CURLOPT_URL => $requestURL,
      CURLOPT_NOBODY => FALSE,
      CURLOPT_HTTPHEADER => array(),
    ));
    // Ensure that any changes to variables in the other thread are picked up.
    $this->refreshVariables();
    $this->verbose('<pre>' . $out . '</pre>');
    return $out;
  }
}

class MAStAdminTest extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => t('MASt Admin tests'),
      'description' => t('Tests the admin UI for the MASt module.'),
      'group' => t('MASt'),
    );
  }

  function setUp() {
    $modules = array('field_test', 'mast');
    parent::setUp($modules);
    $admin_user = $this->drupalCreateUser(array('access administration pages', 'administer site configuration', 'access content', 'administer content types', 'administer taxonomy'));
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests enabling Mobile App Services on the content type edit page.
   */
  function testMastContentTypeEdit() {
    // Test that the "Mobile App Services" fieldset appears on the article edit
    // page
    $this->drupalGet('admin/structure/types/manage/article');
    $this->assertText('Mobile App Services', t('The Mobile App Servies fieldset appears on the article content type edit page'));

    // Confirm that the article content type is not MASt enabled.
    $enabled = mast_enabled('node', 'article');
    $this->assertFalse($enabled, t('The article content type is not MASt enabled.'));

    // Test that we can MASt enable the article content type.
    $edit = array(
      'mast[enable]' => 1,
    );
    $this->drupalPost('admin/structure/types/manage/article', $edit, t('Save content type'));
    $enabled = mast_enabled('node', 'article');
    $this->assertTrue($enabled, t('The article content type was successfully MASt enabled.'));
  }

  /**
   * Tests the effect on MASt eligibility of adding unsupported field types to
   * content types.
   */
  function testAddUnsupportedField() {
    $eligible = mast_bundle_is_eligible('node', 'page');
    $this->assertTrue($eligible, t('The Page content type is eligible to be MASt-enabled'));
    $edit = array(
      'fields[_add_new_field][label]' => $this->randomName(),
      'fields[_add_new_field][field_name]' => 'test_field',
      'fields[_add_new_field][type]' => 'test_field',
      'fields[_add_new_field][widget_type]' => 'test_field_widget',
    );
    $label = $edit['fields[_add_new_field][label]'];
    $field_name = $edit['fields[_add_new_field][field_name]'];

    // First step : 'Add existing field' on the 'Manage fields' page.
    $this->drupalPost("admin/structure/types/manage/page/fields", $edit, t('Save'));
    // Second step : 'Field settings' form.
    $this->drupalPost(NULL, array(), t('Save field settings'));
    // Third step : 'Instance settings' form.
    $this->drupalPost(NULL, array(), t('Save settings'));
    $this->assertRaw(t('Saved %label configuration.', array('%label' => $label)), t('Redirected to "Manage fields" page.'));

    // Check that the field appears in the overview form.
    $this->assertFieldByXPath('//table[@id="field-overview"]//td[1]', $label, t('Instance of unsupported field type was added.'));

    // Clear the field_info cache.
    _field_info_collate_fields(TRUE);
    // Clear the static cache of field types for this bundle.
    drupal_static_reset('mast_bundle_get_unique_field_types');
    // The bundle should no longer be eligible.
    $eligible = mast_bundle_is_eligible('node', 'page');
    $this->assertFalse($eligible, t('The Page content type is no longer eligible to be MASt-enabled'));
  }

  /**
   * Tests attempting to add an unsupported field type to a MASt-enabled content
   * type via the UI.
   */
  function testEnableAndAddUnsupportedField() {
    $edit = array(
      'mast[enable]' => 1,
    );
    $this->drupalPost('admin/structure/types/manage/page', $edit, t('Save content type'));
    $enabled = mast_enabled('node', 'page');
    $this->assertTrue($enabled, t('The page content type was successfully MASt enabled.'));
    
    $edit = array(
      'fields[_add_new_field][label]' => $this->randomName(),
      'fields[_add_new_field][field_name]' => 'test_field',
      'fields[_add_new_field][type]' => 'test_field',
      'fields[_add_new_field][widget_type]' => 'test_field_widget',
    );
    $label = $edit['fields[_add_new_field][label]'];
    $field_name = $edit['fields[_add_new_field][field_name]'];

    // First step : 'Add existing field' on the 'Manage fields' page.
    $this->drupalPost("admin/structure/types/manage/page/fields", $edit, t('Save'));

    $this->assertRaw(t('You cannot add a test_field field to this content type unless you remove the "mobile enabled" setting'), t('Could not add unsupported field to MASt-enabled content type via the UI.'));
  }

  /**
   * Tests that programmatically creating and deleting field instances on content
   * types has the correct effect on their eligibility to be MASt-enabled.
   */
  function testProgrammaticFieldChanges() {
    // MASt-enable the article content type
    $this->drupalPost('admin/structure/types/manage/article', array('mast[enable]' => 1), t('Save content type'));
    $enabled = mast_enabled('node', 'article');
    $this->assertTrue($enabled, t('The article content type was successfully MASt enabled.'));

    // Create a field of an unsupported field type
    $field = array(
      'type' => 'test_field',
      'field_name' => 'field_test_field',
      'cardinality' => 1,
    );
    field_create_field($field);
    // Programmatically add an instance of field_shape_field to the article
    // content type.
    $instance = array(
      'field_name' => 'field_test_field',
      'entity_type' => 'node',
      'label' => 'Article test field',
      'bundle' => 'article',
      'required' => FALSE
    );
    field_create_instance($instance);
    // Confirm that the article content type is now no longer MASt enabled.
    $enabled = mast_enabled('node', 'article');
    $this->assertFalse($enabled, t('The article content type is no longer MASt enabled after programmatically adding an unsupported field type.'));
    $eligible = mast_bundle_is_eligible('node', 'article');
    $this->assertFalse($eligible, t('The article content type is no longer eligible to be MASt-enabled'));
    // Test that the MASt fieldset on the content type edit page now contains an
    // explanation about the content type being ineligible to be MASt enabled.
    $this->drupalGet('admin/structure/types/manage/article');
    $this->assertText(t('This content type is not eligible to be mobile enabled because it contains the following incompatible field types: test_field'), t('The MASt fieldset displays a message that the content type is ineligible to be MASt enabled due to the test field'));

    // Remove the test_field instance from the article content type.
    $this->drupalPost('admin/structure/types/manage/article/fields/field_test_field/delete', array(), t('Delete'));
    // Clear the field_info cache.
    _field_info_collate_fields(TRUE);
    // Clear the static cache of field types for this bundle.
    drupal_static_reset('mast_bundle_get_unique_field_types');
    // The bundle should now be eligible.
    $eligible = mast_bundle_is_eligible('node', 'article');
    $this->assertTrue($eligible, t('The article content type is now eligible to be MASt-enabled'));
    // Test that the MASt enable checkbox appears on the article edit page.
    $this->drupalGet('admin/structure/types/manage/article');
    $this->assertText(t('Allow submission through iPhone/Android app'), t('The MASt enable checkbox appears on the article content type edit page'));
  }
}