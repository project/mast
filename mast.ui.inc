<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add a Mobile App Services fieldset to the node type form with a checkbox to
 * MASt-enable the content type.
 */
function mast_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  $form['mast'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mobile App Services'),
    '#description' => t('Expose this content type to a remote app.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
    '#group' => 'additional_settings',
  );

  $bundle = $form['#node_type']->type;
  $mast_bundle_settings = variable_get('mast_bundle_settings', array());
  if (!isset($mast_bundle_settings['node']) || !isset($mast_bundle_settings['node'][$bundle])) {
    $mast_bundle_settings['node'][$bundle] = mast_bundle_is_eligible('node', $bundle);
    variable_set('mast_bundle_settings', $mast_bundle_settings);
  }

  // If this content type is eligible to be MASt-enabled, display the checkbox
  // for enabling it.
  if ($mast_bundle_settings['node'][$bundle]) {
    $form['#submit'][] = 'mast_node_type_form_submit';
    $enabled = mast_enabled('node', $bundle);
    $form['mast']['enable'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow submission through iPhone/Android app'),
      '#description' => t('When enabled, this content type can be submitted through an iPhone App.'),
      '#default_value' => $enabled,
    );
  }
  else {
    $field_types = mast_bundle_get_unique_field_types('node', $bundle);
    $unsupported_types = mast_bundle_ineligible_field_types($field_types);
    $form['mast']['info'] = array(
      '#markup' => t('This content type is not eligible to be mobile enabled because it contains the following incompatible field types: !types', array('!types' => '<em>' . implode(', ', $unsupported_types) . '</em>'))
    );
  }
}

/**
 * Submit handler for node_type_form.
 *
 * Stores whether or not this node type is MASt-enabled.
 */
function mast_node_type_form_submit($form, &$form_state) {
  $value = variable_get('mast_enabled_bundles', array());
  $value['node'][$form_state['values']['type']] = $form_state['values']['mast']['enable'];
  variable_set('mast_enabled_bundles', $value);
}

/**
 * Implements hook_field_create_instance().
 */
function mast_field_create_instance($instance) {
  $field = field_info_field($instance['field_name']);
  if (!mast_field_type_is_eligible($field['type'])) {
    // If this content type had previously been flagged as eligible, we must now
    // flag it as ineligible.
    $mast_bundle_settings = variable_get('mast_bundle_settings', array());
    if (isset($mast_bundle_settings[$instance['entity_type']]) && isset($mast_bundle_settings[$instance['entity_type']][$instance['bundle']])) {
      if ($mast_bundle_settings[$instance['entity_type']][$instance['bundle']]) {
        $mast_bundle_settings[$instance['entity_type']][$instance['bundle']] = 0;
        variable_set('mast_bundle_settings', $mast_bundle_settings);
      }
    }
    // If this content type had previously been set as MASt-enabled, we now need
    // to unset it.
    if (mast_enabled($instance['entity_type'], $instance['bundle'])) {
      $enabled_types = variable_get('mast_enabled_bundles', array());
      $enabled_types[$instance['entity_type']][$instance['bundle']] = 0;
      variable_set('mast_enabled_bundles', $enabled_types);
      drupal_set_message(t('The @bundle @type bundle has been set to not mobile enabled because an incompatible field type has been added to it', array('@bundle' => $instance['bundle'], '@type' => $instance['entity_type'])));
    }
  }
}

/**
 * Implements hook_field_delete_instance().
 */
function mast_field_delete_instance($instance) {
  $field = field_info_field($instance['field_name']);
  if (!mast_field_type_is_eligible($field['type'])) {
    // If this content type had previously been flagged as ineligible, it might
    // now be eligible as an instance of an unsupported field type has just been
    // removed.
    if (mast_bundle_is_eligible($instance['entity_type'], $instance['bundle'])) {
      $mast_bundle_settings = variable_get('mast_bundle_settings', array());
      $mast_bundle_settings[$instance['entity_type']][$instance['bundle']] = 1;
      variable_set('mast_bundle_settings', $mast_bundle_settings);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Adds a validate hook to the "Manage Fields" form for a content type, which
 * prevents the user adding unsupported field types to a MASt-enabled content
 * type.
 */
function mast_form_field_ui_field_overview_form_alter(&$form, &$form_state) {
  $form['#validate'][] = 'mast_field_ui_field_overview_form_validate';
}

/**
 * Validate callback that prevents unsupported field types from being added
 * to a mobile-enabled content type.
 */
function mast_field_ui_field_overview_form_validate($form, &$form_state) {
  if (mast_enabled($form['#entity_type'], $form['#bundle'])) {
    $values = $form_state['values'];
    if (!empty($values['fields']['_add_new_field']['type'])) {
      $type = $values['fields']['_add_new_field']['type'];
      if (!mast_field_type_is_eligible($type)) {
        form_set_error('fields][_add_new_field][field_name', t('You cannot add a @type field to this content type unless you remove the "mobile enabled" setting', array('@type' => $type)));
      }
    }
    if (!empty($values['fields']['_add_existing_field']['field_name'])) {
      $field = field_info_field($values['fields']['_add_existing_field']['field_name']);
      $type = $field['type'];
      if (!mast_field_type_is_eligible($type)) {
        form_set_error('fields][_add_existing_field][field_name', t('You cannot add a @type field to this content type unless you remove the "mobile enabled" setting', array('@type' => $type)));
      }
    }
  }
}

/**
 * Implements hook_mast_field_types().
 */
function mast_mast_field_types() {
  return array(
    'text',
    'text_long',
    'text_with_summary',
    'list_integer',
    'list_float',
    'list_boolean',
    'list_text',
    'image',
    'file',
    'taxonomy_term_reference',
    'number_integer',
    'number_decimal',
    'number_float',
    'datetime',
    'date',
    'datestamp'
  );
}

/**
 * Checks whether a bundle is MASt enabled.
 * 
 * @param String $entity_type
 *  The machine name of the entity type.
 *
 * @param String $bundle
 *  The machine name of the bundle.
 *
 * Returns TRUE if the bundle is MASt-enabled, FALSE otherwise.
 */
function mast_enabled($entity_type, $bundle) {
  $value = variable_get('mast_enabled_bundles', array());
  if (isset($value[$entity_type]) && isset($value[$entity_type][$bundle])) {
    return $value[$entity_type][$bundle];
  }
  return FALSE;
}

/**
 * Checks whether a bundle is eligible to be MASt-enabled.
 *
 * @param String $entity_type
 *  The machine name of the entity type.
 *
 * @param String $bundle
 *  The machine name of the bundle.
 *
 * Returns FALSE if the bundle contains field types that are not supported, TRUE
 * otherwise.
 */
function mast_bundle_is_eligible($entity_type, $bundle) {
  // Check whether the content type contains field types that prevent it from
  // being eligible to be MASt-enabled.
  $field_types = mast_bundle_get_unique_field_types($entity_type, $bundle);
  $ineligible_types = mast_bundle_ineligible_field_types($field_types);
  return empty($ineligible_types);
}

/**
 * Returns the full list of field types that are supported by MASt and other
 * modules implementing hook_mast_field_types().
 */
function mast_services_get_eligible_field_types() {
  $types = module_invoke_all('mast_field_types');
  return $types;
}

/**
 * Returns an array containing the unique field types used by a given bundle.
 */
function mast_bundle_get_unique_field_types($entity_type, $bundle) {
  $field_types = &drupal_static(__FUNCTION__, array());
  if (empty($field_types) || !isset($field_types[$entity_type]) || !isset($field_types[$entity_type][$bundle])) {
    $instances = field_info_instances($entity_type, $bundle);
    $types = array();
    foreach ($instances as $instance) {
      $field = field_info_field($instance['field_name']);
      $types[] = $field['type'];
    }
    $field_types[$entity_type][$bundle] = array_unique($types);
  }
  return $field_types[$entity_type][$bundle];
}

/**
 * Takes an array of field types and returns an array containing those that are
 * not supported.
 */
function mast_bundle_ineligible_field_types($field_types) {
  $ineligible_types = array();
  foreach ($field_types as $type) {
    if (!mast_field_type_is_eligible($type)) {
      $ineligible_types[] = $type;
    }
  }
  return $ineligible_types;
}

/**
 * Returns TRUE if the passed in field type is supported by the MASt module or
 * by another module that implements hook_mast_field_types().
 */
function mast_field_type_is_eligible($type) {
  $types = &drupal_static(__FUNCTION__, array());
  if (empty($types)) {
    $types = mast_services_get_eligible_field_types();
  }
  return in_array($type, $types);
}